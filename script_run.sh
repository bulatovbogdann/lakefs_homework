docker-compose up

conda env create -f env.yml
source activate lakefs_env

docker exec lakefs-example-lakefs-1 lakectl repo create lakefs://homework s3://homework/

touch data.csv; echo 'type, value' > data.csv; echo 'raw, foo' >> data.csv

docker cp workflow/data/data_ver1.csv lakefs-example-lakefs-1:/home/app #  workflow\data\data_ver2.csv 

docker exec lakefs-example-lakefs-1 lakectl fs upload lakefs://homework/main/data.csv -s data.csv

docker cp data.csv lakefs-example-lakefs-1:/data.csv 
